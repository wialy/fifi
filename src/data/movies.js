import faker from 'faker'

const movies = []

for (let i = 0; i < 10; i++) {
  const movie = {
    id: faker.random.uuid(),
    title: faker.company.catchPhrase(),
    summary: faker.hacker.phrase(),
    rating: Math.random() * 100 / 10,
    imageUrl: require(`../assets/posters/${i % 10}.jpg`)
  }

  movies.push(movie)
}

export default movies

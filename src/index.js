import React from 'react'
import ReactDOM from 'react-dom'

import App from './components/App'
import './css/style.css'

import { createStore, compose, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'

import reducers from './reducers'

import { updateMovies } from './actions'

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose

const middleware = [thunkMiddleware]
const enhancer = composeEnhancers(applyMiddleware(...middleware))

const store = createStore(reducers, enhancer)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
  () => {
    store.dispatch(updateMovies())
  }
)

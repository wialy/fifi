import React from 'react'

export default function Splash({ visible }) {
  return (
    <div className={`splash`}>
      <img src={require('../assets/fifi.png')} alt="fifi" />
      <h1>FiFi</h1>
      <div>Your Film Finder</div>
    </div>
  )
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setSection } from '../actions'

function Item({ label, icon, onClick, selected }) {
  return (
    <div className={`item ${selected ? 'selected' : ''}`} onClick={onClick}>
      <div className="icon">
        <img src={require(`../assets/${icon}.svg`)} alt="" />
      </div>
      <div className="label">{label}</div>
    </div>
  )
}

class Navmenu extends Component {
  items = {
    liked: { label: 'Liked', icon: 'heart' },
    explore: { label: 'Search', icon: 'smell' },
    banned: { label: 'Banned', icon: 'banned' }
  }
  onClickItem = key => this.props.dispatch(setSection(key))
  render() {
    const { items, props } = this
    const { app } = props
    const { section } = app
    return (
      <div className="flex row jc navmenu">
        <div className="container flex row">
          {Object.keys(items).map(key => (
            <Item
              key={key}
              selected={section === key}
              onClick={e => this.onClickItem(key)}
              label={items[key].label}
              icon={items[key].icon}
            />
          ))}
        </div>
      </div>
    )
  }
}

export default connect(({ app }) => ({ app }))(Navmenu)

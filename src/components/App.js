import React, { Component } from 'react'

import Splash from './Splash'
import Navmenu from './Navmenu'
import Explore from './Explore'
import Votes from './Votes'

import { connect } from 'react-redux'

class App extends Component {
  state = {
    splash: true
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({ splash: false })
    }, 1500)
  }
  render() {
    const { splash } = this.state
    const { app } = this.props
    const { section } = app
    return splash ? (
      <Splash />
    ) : (
      <div className="app">
        <Navmenu />
        <div className="flex row jc fluid">
          <div className="container flex column">
            <div className="content fluid padded">
              {section === 'explore' && <Explore />}
              {section === 'liked' && <Votes value={true} />}
              {section === 'banned' && <Votes value={false} />}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(({ app }) => ({ app }))(App)

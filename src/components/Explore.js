import React, { Component } from 'react'
import { connect } from 'react-redux'
import Hammer from 'react-hammerjs'

import { findMovie, vote } from '../actions'

class Explore extends Component {
  state = {
    maxPanDistance: 100,
    vote: '',
    hidden: true
  }
  componentDidMount() {
    const { user, dispatch } = this.props
    const { movie } = user

    if (!movie) {
      dispatch(findMovie())
    }

    this.setState({ hidden: false })
  }
  onPanStart = e => {
    this.setState({ panMovie: this.props.user.movie, panDistance: 0 })
  }
  onPan = e => {
    if (this.state.panMovie && this.state.panMovie !== this.props.user.movie)
      return void 0

    if (e.deltaX <= -this.state.maxPanDistance) {
      this.setState({ panDistance: 0 })
      this.onClickLike()
    } else if (e.deltaX >= this.state.maxPanDistance) {
      this.setState({ panDistance: 0 })
      this.onClickBan()
    } else {
      this.setState({ panDistance: e.deltaX })
    }
  }
  onPanEnd = e => {
    this.setState({
      panDistance: 0,
      panMovie: null
    })
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.user.movie !== this.props.user.movie) {
      this.setState({ hidden: true })
      clearTimeout(this._showTimeout)
      this._showTimeout = setTimeout(() => {
        this.setState({ panDistance: 0, vote: '', hidden: false })
      }, 10)
    }
  }
  onClickLike = e => {
    this.props.dispatch(vote(this.props.user.movie, true))
    this.setState({ vote: 'liked' })
    this.next()
  }
  onClickBan = e => {
    this.props.dispatch(vote(this.props.user.movie, false))
    this.setState({ vote: 'banned' })
    this.next()
  }
  onClickShow = e => {
    this.props.dispatch({ type: 'app:set-section', payload: 'liked' })
  }
  next() {
    clearTimeout(this._nextTimeout)
    this._nextTimeout = setTimeout(() => {
      this.setState({ vote: '', panDistance: 0 })
      this.props.dispatch(findMovie())
    }, 300)
  }
  componentWillUnmount() {
    clearTimeout(this._nextTimeout)
    clearTimeout(this._showTimeout)
  }
  setRef = ref => {
    if (ref) {
      this.setState({ maxPanDistance: ref.offsetWidth / 2 })
    }
  }
  render() {
    const { movies, user } = this.props

    const { movie } = user

    let movieData
    if (movie) {
      movieData = movies.items.find(v => v.id === movie)
    }

    const { vote, hidden, panDistance, maxPanDistance } = this.state
    const locked = vote !== '' || hidden

    return (
      <div
        className={`section explore ${locked ? 'locked' : ''}`}
        ref={this.setRef}>
        {movies.isUpdating && <div className="empty">Loading</div>}
        {!movies.isUpdating &&
          !movieData && (
            <div className="empty">
              <div className="empty">
                <img src={require('../assets/dog-dish.svg')} alt="fifi" />
                We have no more film proposals for you
                <div className="button" onClick={this.onClickShow}>
                  Show my likes
                </div>
              </div>
            </div>
          )}
        {!movies.isUpdating &&
          movieData && (
            <React.Fragment>
              <Hammer
                direction={'DIRECTION_HORIZONTAL'}
                onPan={this.onPan}
                onPanStart={this.onPanStart}
                onPanEnd={this.onPanEnd}
                options={{
                  enable: true
                }}>
                <div className="flex fluid">
                  <div
                    key={movieData.id}
                    style={
                      panDistance !== 0
                        ? {
                            transform: `translateX(${panDistance}px)`,
                            transition: 'none'
                          }
                        : void 0
                    }
                    className={`movie padded ${vote} ${
                      hidden ? 'hidden' : ''
                    } `}>
                    <div className="title">{movieData.title}</div>
                    <div
                      className={`rating ${
                        movieData.rating < 5 ? 'bad' : 'good'
                      }`}>
                      ★ {movieData.rating.toFixed(1)}
                    </div>
                    <div className="photo" draggable={false}>
                      <img src={movieData.imageUrl} alt="" />
                    </div>
                    <div className="summary">{movieData.summary}</div>
                  </div>
                </div>
              </Hammer>

              <div className={`actions flex row jc`}>
                <div
                  className={`button like ${vote === 'liked' ? 'active' : ''} `}
                  style={
                    panDistance !== 0
                      ? {
                          transform: `scale(${1 -
                            panDistance / maxPanDistance})`,
                          transition: 'none'
                        }
                      : void 0
                  }
                  onClick={this.onClickLike}>
                  <div className="icon">
                    <img
                      src={require('../assets/heart-inverted.svg')}
                      alt="like"
                    />
                  </div>
                </div>
                <div
                  className={`button ban ${vote === 'banned' ? 'active' : ''}`}
                  style={
                    panDistance !== 0
                      ? {
                          transform: `scale(${1 +
                            panDistance / maxPanDistance})`,
                          transition: 'none'
                        }
                      : void 0
                  }
                  onClick={this.onClickBan}>
                  <div className="icon">
                    <img
                      src={require('../assets/banned-inverted.svg')}
                      alt="like"
                    />
                  </div>
                </div>
              </div>
            </React.Fragment>
          )}
      </div>
    )
  }
}

export default connect(({ movies, user }) => ({ movies, user }))(Explore)

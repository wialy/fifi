import React, { Component } from 'react'

import { connect } from 'react-redux'

class Votes extends Component {
  remove = id => {
    this.props.dispatch({ type: 'user:unvote', payload: id })
  }
  onClickBrowse = () => {
    this.props.dispatch({ type: 'app:set-section', payload: 'explore' })
  }
  render() {
    const { user, movies, value } = this.props

    if (!movies.items) {
      return <div />
    }

    const list = Object.entries(user.votes)
      .filter(a => a[1] === value)
      .map(a => {
        return movies.items.find(movie => movie.id === a[0])
      })

    return (
      <div className="section votes">
        <h1>Movies you {value ? 'liked' : 'banned'}</h1>

        {list &&
          list.length > 0 && (
            <ul className="fluid scrollable">
              {list.map(v => (
                <li key={v.id}>
                  <div className="title">{v.title}</div>
                  <div className="action" onClick={e => this.remove(v.id)}>
                    ✖
                  </div>
                </li>
              ))}
            </ul>
          )}
        {(!list || list.length === 0) && (
          <div className="empty">
            <img src={require('../assets/fifi.png')} alt="fifi" />
            Nothing here yet
            <div className="button" onClick={this.onClickBrowse}>
              Fifi, search!
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default connect(({ user, movies }) => ({ user, movies }))(Votes)

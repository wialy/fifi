import movies from '../data/movies'

export const setSection = section => ({
  type: 'app:set-section',
  payload: section
})

export const updateMovies = () => (dispatch, getState) => {
  dispatch({
    type: 'movies:update',
    meta: { status: 'start' }
  })

  // fake timeout to simulate GET request
  setTimeout(() => {
    const response = movies
    dispatch({
      type: 'movies:update',
      payload: response,
      meta: { status: 'complete' }
    })
    dispatch(findMovie())
  }, 2000)
}

export const findMovie = () => (dispatch, getState) => {
  const { movies, user } = getState()

  if (movies.items) {
    const unvoted = movies.items.filter(v => user.votes[v.id] == null)
    if (unvoted.length > 0) {
      dispatch({
        type: 'user:set-movie',
        payload: unvoted[0].id
      })
    } else {
      dispatch({
        type: 'user:set-movie',
        payload: null
      })
    }
  }
}

export const vote = (movie, doesLike) => {
  const requestUrl = `/recommendations/${movie}/${
    doesLike ? 'accept' : 'reject'
  }`

  // TODO: send PUT request to requestUrl

  return {
    type: 'user:vote',
    payload: doesLike,
    meta: {
      movie
    }
  }
}

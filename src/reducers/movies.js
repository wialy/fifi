const initialState = {
  isUpdating: false,
  error: null,
  items: []
}

export default (state = initialState, action) => {
  let result = state
  switch (action.type) {
    case 'movies:update':
      switch (action.meta.status) {
        case 'start':
          result = {
            ...state,
            isUpdating: true,
            error: null
          }
          break
        case 'error':
          result = {
            ...state,
            isUpdating: false,
            error: action.payload
          }
          break
        case 'complete':
          result = {
            ...state,
            isUpdating: false,
            items: action.payload
          }
        // no default
      }
      break
    // no default
  }
  return result
}

const initialState = {
  votes: {}
}

export default (state = initialState, action) => {
  let result = state
  switch (action.type) {
    case 'user:vote':
      result = {
        ...state,
        votes: {
          ...state.votes,
          [action.meta.movie]: action.payload
        }
      }
      break

    case 'user:unvote':
      let votes = { ...result.votes }
      delete votes[action.payload]
      result = {
        ...state,
        votes
      }
      break

    case 'user:set-movie':
      result = {
        ...state,
        movie: action.payload
      }
      break

    // no default
  }
  return result
}

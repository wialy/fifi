const initialState = {
  section: 'explore'
}

export default (state = initialState, action) => {
  let result = state
  switch (action.type) {
    case 'app:set-section':
      result = {
        ...state,
        section: action.payload
      }
      break
    // no default
  }
  return result
}

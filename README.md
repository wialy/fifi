# FiFi - Your Film Finder

## Development

### Install dependencies

`yarn install`

### Watch

`yarn start`

### Production build

`yarn build`
